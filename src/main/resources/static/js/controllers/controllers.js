var interviewApp = angular.module('interviewApp', []);

function findAllPlanets($scope, $http) {

	$scope.planets = [];

	$scope.loadPlanets = function () {
		var httpRequest = $http({
			method: 'GET',
			url: '/planets',
			data: mockDataForThisTest

		}).success(function (data, status) {
			$scope.planets = data;
		});

	};

}

interviewApp.controller('interviewController', function ($scope, $http) {

	$scope.planets = [];
	$scope.volume = "0";

	var httpRequest = $http({
		method: 'GET',
		url: '/planets',

	}).success(function (data, status) {
		$scope.planets = data;
	});

});