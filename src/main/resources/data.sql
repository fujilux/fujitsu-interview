INSERT INTO planets(id, name,radius) VALUES (1, 'SUN', 695000);
INSERT INTO planets(id, name,radius) VALUES (2, 'MERCURY', 2440);
INSERT INTO planets(id, name,radius) VALUES (3, 'VENUS', 6052);
INSERT INTO planets(id, name,radius) VALUES (4, 'DEATH STAR', 60);
INSERT INTO planets(id, name,radius) VALUES (5, 'EARTH', 6378);
INSERT INTO planets(id, name,radius) VALUES (6, 'MARS', 3397);
INSERT INTO planets(id, name,radius) VALUES (7, 'JUPITER', 71492);
INSERT INTO planets(id, name,radius) VALUES (8, 'SATURN', 60268);
INSERT INTO planets(id, name,radius) VALUES (9, 'URANUS', 25559);
INSERT INTO planets(id, name,radius) VALUES (10, 'NEPTUNE', 24766);
INSERT INTO planets(id, name,radius) VALUES (11, 'PLUTO', 1150);