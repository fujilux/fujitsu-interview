package lu.fujitsu.ts.interview.application.controller;

import lu.fujitsu.ts.interview.application.service.ApplicationService;
import lu.fujitsu.ts.interview.application.validator.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @Autowired
    ApplicationService applicationService;

    @RequestMapping("/congratulation")
    public String congratulation(@RequestParam(value="volume", required=false) String volume, Model model) {


        EmailValidator emailValidator = new EmailValidator();

        String uncrypEmail = applicationService.getEmail(volume);

        model.addAttribute("email", uncrypEmail);

        if (emailValidator.validate(uncrypEmail)) {
            model.addAttribute("title", "Congratulations");
            model.addAttribute("filename", "death-star.jpeg");
        }else{
            model.addAttribute("title", "Failed");
            model.addAttribute("filename", "failed.jpg");
        }

        return "congratulation";
    }

}