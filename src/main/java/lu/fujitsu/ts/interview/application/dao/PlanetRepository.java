package lu.fujitsu.ts.interview.application.dao;

import lu.fujitsu.ts.interview.application.model.Planet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlanetRepository extends CrudRepository<Planet, Integer> {
}