package lu.fujitsu.ts.interview.application;

import lu.fujitsu.ts.interview.application.service.ApplicationService;
import lu.fujitsu.ts.interview.application.service.impl.ApplicationServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * Main application Class.
 *
 * Prepare Spring beans for the application.
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /**
     * Create an EmailService bean.
     *
     * @return An emailService bean.
     */
    @Bean
    public ApplicationService getApplicationService() {
        return new ApplicationServiceImpl();
    }
}
