package lu.fujitsu.ts.interview.application.service;

import lu.fujitsu.ts.interview.application.model.Planet;

/**
 * EmailService interface.
 */
public interface ApplicationService {

    /**
     * Get the email to send CV.
     *
     * @return String email
     * @param volume
     */
    String getEmail(String volume);

    Iterable<Planet> findAllPlanets();
}
