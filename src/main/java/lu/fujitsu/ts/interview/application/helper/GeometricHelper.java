package lu.fujitsu.ts.interview.application.helper;

/**
 * Created by eric on 17/04/16.
 */
public class GeometricHelper {

    /**
     * Compute a sphere volume based on sphere radius
     *
     * @param radius Sphere radius
     * @return
     */
    public static double computeVolume(final double radius) {
        // TODO write here the volume compute
        //return 0;

        return (4.0 * Math.PI * Math.pow(radius, 3.0))/3.0;
    }
}
