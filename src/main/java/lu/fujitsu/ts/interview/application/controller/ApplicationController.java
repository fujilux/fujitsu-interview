package lu.fujitsu.ts.interview.application.controller;

import lu.fujitsu.ts.interview.application.model.Planet;
import lu.fujitsu.ts.interview.application.service.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApplicationController {

    @Autowired
    ApplicationService applicationService;

    @RequestMapping(value = "/planets", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Planet> findAllPlanets() {

        // TODO 3 : Use the GeometricHelper class previously tested to compute the volume for all planets before returning them.
        return applicationService.findAllPlanets();
    }
}
