package lu.fujitsu.ts.interview.application.service.impl;

import lu.fujitsu.ts.interview.application.dao.PlanetRepository;
import lu.fujitsu.ts.interview.application.model.Planet;
import lu.fujitsu.ts.interview.application.service.ApplicationService;
import org.jasypt.util.text.BasicTextEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class ApplicationServiceImpl implements ApplicationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationServiceImpl.class);

    @Autowired
    private PlanetRepository planetRepository;

    @Value("${email.encrypted}")
    private String email;

    @Override
    public String getEmail(final String password){
        String uncryptemail = "";
        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword(password);

        try {
            uncryptemail = textEncryptor.decrypt(email);
        } catch (Exception e) {
            LOGGER.error("An error occured during email decryption for email {} and password {}",email,password);
        }

        return uncryptemail;
    }

    @Override
    public Iterable<Planet> findAllPlanets() {
        return planetRepository.findAll();
    }


}
