package lu.fujitsu.ts.interview.application.helper;

import org.junit.Test;

import static junit.framework.TestCase.fail;

/**
 * Execute unit tests for the GeometricHelper class.
 */
public class GeometricHelperTests {

    // Russian billiard ball properties :
    // radius in millimeters
    private static double RUSSIAN_POOL_BALL_RADIUS = 34.0;
    // Expected volume
    private static double RUSSIAN_POOL_BALL_VOLUME = 164636.0;

    @Test
    public void testComputeVolume() {
        // TODO 2 Write a unit test that check the GeometricHelper.computeVolume method compute correctly the volume of a Russian pool ball (the expected result is RUSSIAN_POOL_BALL_VOLUME constant)
        fail("Write the TODO 2 unit test");
    }
}
