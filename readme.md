# Getting started

The goal of fujitsu-interview project is to evaluate several skills through a real and fun project:
 
- Ability to read documentation
- ability to communicate when a problem is encountered
- Technical skills ( Git, Java, Angular, Spring...)

Included in this present writing https:..daringfireball.net.projects.markdown. file all necessary information to perform the test.

Let's go !

## What you’ll need
    
A favorite text editor or IDE
JDK 1.6 or later
Maven 3.0+

## How to package and run the application

Packaging :
mvn package 

Run:
java -jar target.fujitsu-interview-0.0.1.jar

or 

execute the main lu.fujitsu.ts.interview.application.Application class

## Pass the tests
### TODO 1 : find the right dependency to compile the project correctly

Having cloned the Git project, it does not compile a dependency is missing. This is the test of TODO 1, find it and save the world.

### TODO 2 Write a unit test

Write a unit test that check the GeometricHelper.computeVolume method compute correctly the volume of a Russian pool ball (the expected result is RUSSIAN_POOL_BALL_VOLUME constant).
The GeometricHelperTests where you will write the test already exists.

### TODO 3 : Take the control and show it

Modify the ApplicationController and use the GeometricHelper class previously tested to compute the volume for all planets before returning them.
Display the computed volume in the index page and follow the explanations at the bottom page.
           
THE END